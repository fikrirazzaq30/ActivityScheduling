package com.example.telc2.activityscheduling.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.telc2.activityscheduling.activity.anak.HomeAnakEuyActivity;
import com.example.telc2.activityscheduling.activity.ortu.AturPinActivity;
import com.example.telc2.activityscheduling.activity.ortu.HomeActivity;

public class SplashScreenActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_splash_screen);

//        Thread timerThread = new Thread(){
//            public void run(){
//                try{
//                    sleep(1000);
//                }catch(InterruptedException e){
//                    e.printStackTrace();
//                }finally{

                    sharedPreferences = getSharedPreferences("ShaPreferences", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    boolean  firstTime = sharedPreferences.getBoolean("first", true);
                    if(firstTime) {
                        editor.putBoolean("first",false);
                        //For commit the changes, Use either editor.commit(); or  editor.apply();.
                        editor.apply();
                        Intent intent = new Intent(SplashScreenActivity.this, AturPinActivity.class);
                        startActivity(intent);
                    }
                    else
                    {
                        Intent intent = new Intent(SplashScreenActivity.this, HomeAnakEuyActivity.class);
                        startActivity(intent);
                    }

                    finish();
//                }
//            }
//        };
//        timerThread.start();
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        finish();
    }
}
