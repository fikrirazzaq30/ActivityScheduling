package com.example.telc2.activityscheduling.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.model.NotifModel;

import java.util.List;

/**
 * Created by TELC2 on 7/16/2017.
 */

public class PemberitahuanAdapter extends RecyclerView.Adapter<PemberitahuanAdapter.HolderData>{

    List<NotifModel> notifModelList;
    Context mContext;

    public PemberitahuanAdapter(List<NotifModel> notifModelList, Context mContext) {
        this.notifModelList = notifModelList;
        this.mContext = mContext;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_notif, parent, false);
        PemberitahuanAdapter.HolderData holder = new PemberitahuanAdapter.HolderData(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        holder.txNotif.setText(notifModelList.get(position).getNotifikasi());
        holder.txWaktuTgl.setText(notifModelList.get(position).getWaktu().substring(0, 8));
        holder.txWaktuJam.setText(notifModelList.get(position).getWaktu().substring(9).substring(0, 5));
    }

    @Override
    public int getItemCount() {
        return notifModelList.size();
    }

    class HolderData extends RecyclerView.ViewHolder {

        TextView txNotif, txWaktuTgl, txWaktuJam;

        public HolderData(View itemView) {
            super(itemView);
            txNotif = (TextView) itemView.findViewById(R.id.tx_notif);
            txWaktuTgl = (TextView) itemView.findViewById(R.id.tx_waktu_notif_tgl);
            txWaktuJam = (TextView) itemView.findViewById(R.id.tx_waktu_notif_jam);
        }
    }
}
