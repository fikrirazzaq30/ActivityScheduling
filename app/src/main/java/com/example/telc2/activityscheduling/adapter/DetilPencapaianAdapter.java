package com.example.telc2.activityscheduling.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.model.PencapaianModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by TELC2 on 7/11/2017.
 */

public class DetilPencapaianAdapter extends RecyclerView.Adapter<DetilPencapaianAdapter.ViewHolder> {

    private ArrayList<PencapaianModel> mListObjects = new ArrayList<>();
    private Context mContext;


    public DetilPencapaianAdapter(ArrayList<PencapaianModel> listObjects, Context context) {
        this.mListObjects = listObjects;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_capaian, null);

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        //viewHolder.txtViewTitle.setText(mListObjects.get(position).getTitle());
        Picasso.with(mContext)
                .load(mListObjects.get(position).getImgUrl())
                .resize(500, 500)
                .centerCrop()
                .into(viewHolder.imgViewIcon);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtViewTitle;
        private ImageView imgViewIcon;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            //txtViewTitle = (TextView) itemLayoutView.findViewById(R.id.item_title);
            imgViewIcon = (ImageView) itemLayoutView.findViewById(R.id.img_pencapaian);
        }
    }


    @Override
    public int getItemCount() {
        return mListObjects.size();
    }
}
