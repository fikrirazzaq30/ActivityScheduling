package com.example.telc2.activityscheduling.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by TELC2 on 7/9/2017.
 */

public class RetroServer {

    public static final String base_url = "http://juvetic.telclab.com/actscdhl/";

    private static Retrofit retrofit;

    public static Retrofit getClient() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(base_url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }

    public static ApiRequest getRequestService() {
        return getClient().create(ApiRequest.class);
    }
}
