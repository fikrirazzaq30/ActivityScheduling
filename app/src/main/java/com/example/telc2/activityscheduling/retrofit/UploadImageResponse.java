package com.example.telc2.activityscheduling.retrofit;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TELC2 on 7/12/2017.
 */

public class UploadImageResponse {

    // variable name should be same as in the json response from php
    // @SerializedName("success")

    boolean success;
    @SerializedName("message")
    String message;

    public String getMessage() {
        return message;
    }

    public boolean getSuccess() {
        return success;
    }
}
