package com.example.telc2.activityscheduling.model;

/**
 * Created by TELC2 on 7/10/2017.
 */

public class NotifModel {

    String notifikasi;
    String waktu;
    String akt_id;

    public String getNotifikasi() {
        return notifikasi;
    }

    public void setNotifikasi(String notifikasi) {
        this.notifikasi = notifikasi;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getAkt_id() {
        return akt_id;
    }

    public void setAkt_id(String akt_id) {
        this.akt_id = akt_id;
    }
}
