package com.example.telc2.activityscheduling.model;

import android.media.Image;

import java.util.List;

/**
 * Created by juvetic on 6/22/17.
 */

public class AktivitasModel {

    private String akt_id;
    private String akt_judul;
    private String akt_gambar;
    private String akt_waktu;
    private String akt_capaian;
    private String akt_kerjakan;

    public String getAkt_kerjakan() {
        return akt_kerjakan;
    }

    public void setAkt_kerjakan(String akt_kerjakan) {
        this.akt_kerjakan = akt_kerjakan;
    }

    public String getAkt_id() {
        return akt_id;
    }

    public void setAkt_id(String akt_id) {
        this.akt_id = akt_id;
    }

    public String getAkt_judul() {
        return akt_judul;
    }

    public void setAkt_judul(String akt_judul) {
        this.akt_judul = akt_judul;
    }

    public String getAkt_gambar() {
        return akt_gambar;
    }

    public void setAkt_gambar(String akt_gambar) {
        this.akt_gambar = akt_gambar;
    }

    public String getAkt_waktu() {
        return akt_waktu;
    }

    public void setAkt_waktu(String akt_waktu) {
        this.akt_waktu = akt_waktu;
    }

    public String getAkt_capaian() {
        return akt_capaian;
    }

    public void setAkt_capaian(String akt_capaian) {
        this.akt_capaian = akt_capaian;
    }
}