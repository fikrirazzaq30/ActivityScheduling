package com.example.telc2.activityscheduling.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.activity.anak.HomeAnakEuyActivity;
import com.example.telc2.activityscheduling.activity.ortu.HomeActivity;
import com.example.telc2.activityscheduling.activity.ortu.LupaPinActivity;
import com.example.telc2.activityscheduling.activity.ortu.MasukOrtuActivity;

import static com.example.telc2.activityscheduling.R.id.card_mode_anak;

public class SettingsActivity extends AppCompatActivity {

    private CardView cardViewModeOrtu, cardViewModeAnak;
    private TextView txStatusOrtu, txStatusAnak;
    private Button btnLupaPin;
    private int statusAnak = 0;
    private int statusOrtu = 0;
    LinearLayout linearAnak;
    LinearLayout linearOrtu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setTitle("Pengaturan");

        cardViewModeOrtu = (CardView) findViewById(R.id.card_mode_ortu);
        cardViewModeAnak = (CardView) findViewById(card_mode_anak);
        txStatusAnak = (TextView) findViewById(R.id.tx_status_anak);
        txStatusOrtu = (TextView) findViewById(R.id.tx_status_ortu);
        btnLupaPin = (Button) findViewById(R.id.btn_lupa_pin_setting);
        linearAnak = (LinearLayout) findViewById(R.id.linearAnak);
        linearOrtu = (LinearLayout) findViewById(R.id.linearOrtu);


        cardViewModeAnak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txStatusOrtu.setText("");
                txStatusAnak.setText("Sedang Aktif");
                statusAnak = 1;
                statusOrtu = 0;
                cardViewModeAnak.setCardBackgroundColor(getResources().getColor(R.color.ijo));
                cardViewModeOrtu.setCardBackgroundColor(getResources().getColor(R.color.divider));
                linearOrtu.setBackgroundColor(Color.parseColor("#BDBDBD"));
                linearAnak.setBackgroundColor(Color.parseColor("#1c7b07"));
                Toast.makeText(SettingsActivity.this, "Mode Anak: Aktif", Toast.LENGTH_SHORT).show();
            }
        });

        cardViewModeOrtu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txStatusOrtu.getText().equals("Sedang Aktif")) {
                    cardViewModeAnak.setCardBackgroundColor(getResources().getColor(R.color.divider));
                    cardViewModeOrtu.setCardBackgroundColor(getResources().getColor(R.color.ijo));
                    Toast.makeText(SettingsActivity.this, "Mode Orang Tua: Aktif", Toast.LENGTH_SHORT).show();
                    linearOrtu.setBackgroundColor(Color.parseColor("#1c7b07"));
                    linearAnak.setBackgroundColor(Color.parseColor("#BDBDBD"));
                    Toast.makeText(SettingsActivity.this, "Mode Orang Tua: Aktif", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(SettingsActivity.this, MasukOrtuActivity.class);
                    startActivity(intent);
                }
            }
        });

        btnLupaPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, LupaPinActivity.class);
                startActivity(intent);
            }
        });

        Intent intent = getIntent();
        Bundle b = intent.getExtras();

        if (b != null) {
            statusOrtu = Integer.parseInt((String) b.get("statusOrtu"));
            statusAnak = Integer.parseInt((String) b.get("statusAnak"));

            if (statusAnak == 1 && statusOrtu == 0) {
                txStatusAnak.setText("Sedang Aktif");
                txStatusOrtu.setText("");
                cardViewModeAnak.setCardBackgroundColor(getResources().getColor(R.color.ijo));
                cardViewModeOrtu.setCardBackgroundColor(getResources().getColor(R.color.divider));
                linearOrtu.setBackgroundColor(Color.parseColor("#BDBDBD"));
                linearAnak.setBackgroundColor(Color.parseColor("#1c7b07"));
                Toast.makeText(SettingsActivity.this, "Mode Anak: Aktif", Toast.LENGTH_SHORT).show();
            }
            else if (statusAnak == 0 && statusOrtu == 1) {
                txStatusAnak.setText("");
                txStatusOrtu.setText("Sedang Aktif");
                cardViewModeAnak.setCardBackgroundColor(getResources().getColor(R.color.divider));
                cardViewModeOrtu.setCardBackgroundColor(getResources().getColor(R.color.ijo));
                Toast.makeText(SettingsActivity.this, "Mode Orang Tua: Aktif", Toast.LENGTH_SHORT).show();
                linearOrtu.setBackgroundColor(Color.parseColor("#1c7b07"));
                linearAnak.setBackgroundColor(Color.parseColor("#BDBDBD"));
            }
        }

        BroadcastReceiver broadcast_reciever = new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent intent) {
                String action = intent.getAction();
                if (action.equals("finish_activity")) {
                    finish();
                    // DO WHATEVER YOU WANT.
                }
            }
        };


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            if (statusOrtu == 1 && statusAnak == 0) {
                Intent intent = new Intent(SettingsActivity.this, HomeActivity.class);
                startActivity(intent);
            }
            else if (statusAnak == 1 && statusOrtu == 0) {
                Intent intent = new Intent(SettingsActivity.this, HomeAnakEuyActivity.class);
                startActivity(intent);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
