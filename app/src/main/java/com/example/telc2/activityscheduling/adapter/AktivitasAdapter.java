package com.example.telc2.activityscheduling.adapter;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.model.Aktivitas;
import com.example.telc2.activityscheduling.model.AktivitasModel;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import static com.example.telc2.activityscheduling.R.id.parent;

/**
 * Created by juvetic on 6/22/17.
 */

public class AktivitasAdapter extends RecyclerView.Adapter<AktivitasAdapter.HolderData> {

    List<AktivitasModel> aktivitasList;
    Context mContext;

    public AktivitasAdapter(List<AktivitasModel> aktivitasList, Context mContext) {
        this.aktivitasList = aktivitasList;
        this.mContext = mContext;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_act, parent, false);
        HolderData holder = new HolderData(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        holder.txJudulAktivitas.setText(aktivitasList.get(position).getAkt_judul());
        holder.txWaktuAktivitas.setText(aktivitasList.get(position).getAkt_waktu());
        holder.txKerjakan.setText(aktivitasList.get(position).getAkt_kerjakan());
        Picasso.with(mContext).load(aktivitasList.get(position).getAkt_gambar()).into(holder.imgAktivitas);

        if (aktivitasList.get(position).getAkt_kerjakan().equals("1")) {
            holder.imgCheck.setVisibility(View.VISIBLE);
        }  else {
            holder.imgCheck.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return aktivitasList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, AktivitasModel aktivitasModel) {
        aktivitasList.add(position, aktivitasModel);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(AktivitasModel aktivitasModel) {
        int position = aktivitasList.indexOf(aktivitasModel);
        aktivitasList.remove(position);
        notifyItemRemoved(position);
    }

    class HolderData extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView txJudulAktivitas, txWaktuAktivitas, txKerjakan;
        ImageView imgAktivitas, imgCheck;

        public HolderData(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_item_act);
            txJudulAktivitas = (TextView) itemView.findViewById(R.id.tx_judul_aktivitas);
            txWaktuAktivitas = (TextView) itemView.findViewById(R.id.tx_waktu_aktivitas);
            imgAktivitas = (ImageView) itemView.findViewById(R.id.img_aktivitas_ortu);
            imgCheck = (ImageView) itemView.findViewById(R.id.img_kerjakan_ortu);
            txKerjakan = (TextView) itemView.findViewById(R.id.tx_kerjakan_ortu);
        }
    }
}
