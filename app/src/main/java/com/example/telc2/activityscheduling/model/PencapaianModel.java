package com.example.telc2.activityscheduling.model;

/**
 * Created by TELC2 on 7/2/2017.
 */

public class PencapaianModel {

    private int imgUrl;
    private String namaImage;

    public PencapaianModel(int imgUrl, String namaImage) {
        this.imgUrl = imgUrl;
        this.namaImage = namaImage;
    }

    public int getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(int imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getNamaImage() {
        return namaImage;
    }

    public void setNamaImage(String namaImage) {
        this.namaImage = namaImage;
    }
}
