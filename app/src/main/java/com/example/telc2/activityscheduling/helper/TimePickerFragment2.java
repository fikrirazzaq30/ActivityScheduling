package com.example.telc2.activityscheduling.helper;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.Button;
import android.widget.TimePicker;

import com.example.telc2.activityscheduling.R;

import java.util.Calendar;

/**
 * Created by jahid on 12/10/15.
 */
public class TimePickerFragment2 extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // Do something with the time chosen by the user
        Button btnPilihWaktu = (Button) getActivity().findViewById(R.id.btn_ubah_pilih_waktu);
        btnPilihWaktu.setText(+view.getCurrentHour()+":"+view.getCurrentMinute() + "    Setiap Hari");
    }
}
