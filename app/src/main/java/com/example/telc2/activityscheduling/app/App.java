package com.example.telc2.activityscheduling.app;

import android.app.Application;
import android.content.Context;

/**
 * Created by juvetic on 7/3/17.
 */

public class App extends Application {

    private static Context context;

    @Override
    public void onCreate()
    {
        super.onCreate();
        context = this.getApplicationContext();
    }

    public static Context getContext(){
        return context;
    }
}
