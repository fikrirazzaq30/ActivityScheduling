package com.example.telc2.activityscheduling.retrofit;

import com.example.telc2.activityscheduling.model.response.NotifResponseModel;
import com.example.telc2.activityscheduling.model.response.AktivitasResponseModel;
import com.example.telc2.activityscheduling.model.response.TahapResponseModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by TELC2 on 7/9/2017.
 */

public interface ApiRequest {

//    Insert Aktivitas
    @FormUrlEncoded
    @POST("insert_aktivitas.php")
    Call<AktivitasResponseModel> insertAktivitas(@Field("akt_id") String akt_id,
                                  @Field("akt_judul") String akt_judul,
                                  @Field("akt_gambar") String akt_gambar,
                                  @Field("akt_waktu") String akt_waktu,
                                  @Field("akt_capaian") String akt_capaian,
                                  @Field("akt_terapi_status") String akt_terapi_status);

//    Delete Aktivitas
    @FormUrlEncoded
    @POST("delete_aktivitas.php")
    Call<AktivitasResponseModel> deleteAktivitas(@Field("akt_id") String akt_id);

//    Update Aktivitas
    @FormUrlEncoded
    @POST("update_aktivitas.php")
    Call<AktivitasResponseModel> updateAktivitas(@Field("akt_id") String akt_id,
                                                 @Field("akt_judul") String akt_judul,
                                                 @Field("akt_gambar") String akt_gambar,
                                                 @Field("akt_waktu") String akt_waktu);

    //    Update Aktivitas - Kerjakan
    @FormUrlEncoded
    @POST("update_aktivitas.php")
    Call<AktivitasResponseModel> updateAktivitasKerjakan(@Field("akt_id") String akt_id,
                                                 @Field("akt_kerjakan") String akt_kerjakan);

//    Insert Tahap
    @FormUrlEncoded
    @POST("insert_tahap.php")
    Call<TahapResponseModel> insertTahap(@Field("thp_id") String thp_id,
                                     @Field("thp_nomor") String thp_nomor,
                                     @Field("thp_gambar") String thp_gambar,
                                     @Field("thp_desk") String thp_desk,
                                     @Field("akt_id") String akt_id);

    //    Update Tahap
    @FormUrlEncoded
    @POST("update_tahap.php")
    Call<TahapResponseModel> updateTahap(@Field("thp_id") String thp_id,
                                         @Field("thp_gambar") String thp_gambar,
                                         @Field("thp_desk") String thp_desk);


//    Delete Tahap
    @FormUrlEncoded
    @POST("delete_tahap.php")
    Call<TahapResponseModel> deleteTahap(@Field("akt_id") String akt_id);

//    Update Tahap


//    Get Capaian
    @FormUrlEncoded
    @POST("view_capaian_by_akt_id.php")
    Call<AktivitasResponseModel> getCapaianById(@Field("akt_id") String akt_id);

//    Update Capaian
    @FormUrlEncoded
    @POST("update_capaian.php")
    Call<AktivitasResponseModel> updateCapaian(@Field("akt_capaian") String akt_capaian,
                                               @Field("akt_kerjakan") String akt_kerjakan,
                                               @Field("akt_id") String akt_id);

//    View Aktivitas - All - Home Ortu
    @GET("view_aktivitas_ortu.php")
    Call<AktivitasResponseModel> getAktivitasOrtu();


//    View Aktivitas - All - Home Anak
    @GET("view_aktivitas_anak.php")
    Call<AktivitasResponseModel> getAktivitasAnak();

//    View Tahap By ID Aktivitas
    @FormUrlEncoded
    @POST("view_tahap_by_akt_id.php")
    Call<TahapResponseModel> getTahapById(@Field("akt_id") String akt_id);

//    Insert Notif
    @FormUrlEncoded
    @POST("insert_notif.php")
    Call<NotifResponseModel> insertNotif(@Field("waktu") String waktu,
                                         @Field("notifikasi") String notifikasi,
                                         @Field("akt_id") String akt_id);

//    View Notif
    @GET("view_notif.php")
    Call<NotifResponseModel> getNotif();

//    View Notif by Akt Id
    @FormUrlEncoded
    @POST("view_notif_by_akt_id.php")
    Call<NotifResponseModel> getNotifById(@Field("akt_id") String akt_id);

    //    View Aktivitas by Akt Id
    @FormUrlEncoded
    @POST("view_aktivitas_by_akt_id.php")
    Call<AktivitasResponseModel> getAktById(@Field("akt_id") String akt_id);

//    Upload Image
    @Multipart
    @POST("retrofit_client.php")
    Call<UploadImageResponse> uploadFile(@Part MultipartBody.Part file, @Part("file") RequestBody name);
}
