package com.example.telc2.activityscheduling.activity.anak;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.activity.DetilPencapaianActivity;
import com.example.telc2.activityscheduling.activity.SelesaiAktivitasActivity;
import com.example.telc2.activityscheduling.model.Aktivitas;
import com.example.telc2.activityscheduling.model.response.NotifResponseModel;
import com.example.telc2.activityscheduling.model.TahapModel;
import com.example.telc2.activityscheduling.model.response.AktivitasResponseModel;
import com.example.telc2.activityscheduling.model.response.TahapResponseModel;
import com.example.telc2.activityscheduling.retrofit.ApiRequest;
import com.example.telc2.activityscheduling.retrofit.RetroServer;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.telc2.activityscheduling.activity.anak.DetilTahap.now;

public class DetilTahapActAnakActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {

    private String judulAktivitas, idAktivitas, capaianAktivitas;
    private TextView txJudulTahap, txDeskTahap;
    private ImageView imgGambarTahap;
    private Button btnSelanjutnya, btnSelesai, btnDone;
    private ImageButton btnSuaraTahap;
    private List<TahapModel> tahapModelList, tahapModels;
    private ProgressDialog progressDialog;
    private int countTahap = 0;
    private TextToSpeech tts;
    private Context mContext;
    private CardView cvTahap, cvSelesai;
    private LinearLayout linearSelesai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_tahap_act_anak);
        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null) {
            judulAktivitas = (String) b.get("judulAktivitas");
            idAktivitas = (String) b.get("idAktivitas");
            capaianAktivitas = (String) b.get("capaianAktivitas");
        }

        tts = new TextToSpeech(this, this);
        btnSuaraTahap = (ImageButton) findViewById(R.id.btn_suara_tahap);
        btnSuaraTahap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                speakOut();
            }
        });

        setTitle(judulAktivitas);

        ApiRequest api = RetroServer.getClient().create(ApiRequest.class);
        Call<TahapResponseModel> getTahapById = api.getTahapById(idAktivitas);
        getTahapById.enqueue(new Callback<TahapResponseModel>() {
            @Override
            public void onResponse(Call<TahapResponseModel> call, Response<TahapResponseModel> response) {
                Log.d("Retrofit", "Response: " + response.body().getKode());
                tahapModelList = response.body().getResult();
                tahapModels = new ArrayList<TahapModel>();
                tahapModels = tahapModelList;
                txJudulTahap.setText("Tahap " + tahapModels.get(countTahap).getThp_nomor());
                txDeskTahap.setText(tahapModels.get(countTahap).getThp_desk());

                Picasso.with(mContext).load(tahapModels.get(countTahap).getThp_gambar()).into(imgGambarTahap);

                Log.d("Cek Desk", tahapModels.get(countTahap).getThp_desk());

            }

            @Override
            public void onFailure(Call<TahapResponseModel> call, Throwable t) {
                Log.d("Retrofit", "Failure: Response Gagal");
            }
        });

        txJudulTahap = (TextView) findViewById(R.id.tx_nomor_tahap_anak);
        txDeskTahap = (TextView) findViewById(R.id.tx_desk_tahap_anak);
        imgGambarTahap = (ImageView) findViewById(R.id.img_detil_tahap_anak);
        btnSelanjutnya = (Button) findViewById(R.id.btn_next_tahap_anak);
        btnSelesai = (Button) findViewById(R.id.btn_finish_tahap_anak);
        btnDone = (Button) findViewById(R.id.btn_lihat_pencapaian);
        cvTahap = (CardView) findViewById(R.id.cv_tahap);
        cvSelesai = (CardView) findViewById(R.id.cv_selesai);
        linearSelesai = (LinearLayout) findViewById(R.id.linearSelesai);

        Log.d("Init Count ", "" + countTahap);
        btnSelanjutnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (countTahap < (tahapModels.size())) {
                    txJudulTahap.setText("Tahap " + tahapModels.get(countTahap).getThp_nomor());
                    txDeskTahap.setText(tahapModels.get(countTahap).getThp_desk());
                    Picasso.with(mContext).load(tahapModels.get(countTahap).getThp_gambar()).into(imgGambarTahap);
                    countTahap++;
                    Log.d("Count Checking ", "" + countTahap);
                }
                else {
                    btnSelanjutnya.setVisibility(View.GONE);
                    cvTahap.setVisibility(View.GONE);
                    cvSelesai.setVisibility(View.VISIBLE);
                    linearSelesai.setVisibility(View.VISIBLE);
//                    btnSelesai.setVisibility(View.VISIBLE);
                }
            }
        });

        /**
         * Event Tombol Selesai
         */
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int capai = Integer.parseInt(capaianAktivitas);
                capai++;
                String sCapai = Integer.toString(capai);

                ApiRequest apiTahap = RetroServer.getClient().create(ApiRequest.class);
                Call<AktivitasResponseModel> updateCapaian = apiTahap.updateCapaian(sCapai, "1", idAktivitas);
                updateCapaian.enqueue(new Callback<AktivitasResponseModel>() {
                    @Override
                    public void onResponse(Call<AktivitasResponseModel> call, Response<AktivitasResponseModel> response) {
                        Log.d("Retrofit", "response: " + response.body().toString());
                        String kode = response.body().getKode();
                        if (kode.equals("1")) {
                            NotificationManager notif=(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
                            Notification notify = new Notification.Builder
                                    (getApplicationContext()).setContentTitle("Pemberitahuan").setContentText("Anak Anda menyelesaikan aktivitas " + judulAktivitas).
                                    setContentTitle("Scheduling Activity").setSmallIcon(R.mipmap.ic_launcher).build();

                            notify.flags |= Notification.FLAG_AUTO_CANCEL;
                            notif.notify(0, notify);
                            finish();
                        } else {
                            Toast.makeText(DetilTahapActAnakActivity.this, "Terjadi kesalahan jaringan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<AktivitasResponseModel> call, Throwable t) {
                        progressDialog.dismiss();
                        Log.d("Retrofit", "Failure: " + "Gagal Mengirim Request" );
                    }
                });

                //Insert Notif
                ApiRequest apiNotif = RetroServer.getClient().create(ApiRequest.class);
                Call<NotifResponseModel> insertNotif = apiNotif.insertNotif(now(), "Anak Anda menyelesaikan " + judulAktivitas, idAktivitas);
                insertNotif.enqueue(new Callback<NotifResponseModel>() {
                    @Override
                    public void onResponse(Call<NotifResponseModel> call, Response<NotifResponseModel> response) {
                        Log.d("Retrofit", "injeksi: " + response.body().getKode());
                        Log.d("ISI", "injrasd" + now() + "Anak Anda menyelesaikan " + judulAktivitas + idAktivitas);
                        String kode = response.body().getKode();
                        if (kode.equals("1")) {
                            finish();
                        } else {
                            Toast.makeText(DetilTahapActAnakActivity.this, "Terjadi kesalahan jaringan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<NotifResponseModel> call, Throwable t) {
                        progressDialog.dismiss();
                        Log.d("Retrofit", "Failure: " + "Gagal Mengirim Request" );
                    }
                });

//                //Update Kerjakan
//                ApiRequest apiKerjakan = RetroServer.getClient().create(ApiRequest.class);
//                Call<AktivitasResponseModel> updateKerjakan = apiKerjakan.updateAktivitasKerjakan(idAktivitas, "1");
//                updateKerjakan.enqueue(new Callback<AktivitasResponseModel>() {
//                    @Override
//                    public void onResponse(Call<AktivitasResponseModel> call, Response<AktivitasResponseModel> response) {
//                        String kode = response.body().getKode();
//                        if (kode.equals("1")) {
//                            finish();
//                        } else {
//                            Toast.makeText(DetilTahapActAnakActivity.this, "Terjadi kesalahan jaringan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<AktivitasResponseModel> call, Throwable t) {
//
//                    }
//                });

                Intent intent = new Intent(DetilTahapActAnakActivity.this, DetilPencapaianActivity.class);
                intent.putExtra("judulAktivitas", judulAktivitas);
                intent.putExtra("idAktivitas", idAktivitas);
                intent.putExtra("capAktivitas", capaianAktivitas);
                startActivity(intent);
                finish();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            countTahap--;
            txJudulTahap.setText("Tahap " + tahapModels.get(countTahap).getThp_nomor());
            txDeskTahap.setText(tahapModels.get(countTahap).getThp_desk());
            Picasso.with(mContext).load(tahapModels.get(countTahap).getThp_gambar()).into(imgGambarTahap);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(new Locale("id","ID"));

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                btnSuaraTahap.setEnabled(true);
//                speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }

    private void speakOut() {

        String text = txDeskTahap.getText().toString();

        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown tts!
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }


}
