package com.example.telc2.activityscheduling.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.model.Aktivitas;
import com.example.telc2.activityscheduling.model.AktivitasModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by TELC2 on 7/2/2017.
 */

public class AktivitasAnakAdapter extends RecyclerView.Adapter<AktivitasAnakAdapter.HolderData> {

    List<AktivitasModel> aktivitasModelList;
    Context mContext;

    public AktivitasAnakAdapter(List<AktivitasModel> aktivitasModelList, Context mContext) {
        this.aktivitasModelList = aktivitasModelList;
        this.mContext = mContext;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_act_anak, parent, false);
        HolderData holder = new HolderData(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(AktivitasAnakAdapter.HolderData holder, int position) {
        holder.txJudulAktivitas.setText(aktivitasModelList.get(position).getAkt_judul());
        holder.txWaktuAktivitas.setText(aktivitasModelList.get(position).getAkt_waktu());
        holder.txKerjakan.setText(aktivitasModelList.get(position).getAkt_kerjakan());
        Picasso.with(mContext).load(aktivitasModelList.get(position).getAkt_gambar()).into(holder.imgAktivitas);

        if (aktivitasModelList.get(position).getAkt_kerjakan().equals("1")) {
            holder.imgCheck.setVisibility(View.VISIBLE);
        }  else {
            holder.imgCheck.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return aktivitasModelList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    class HolderData extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView txJudulAktivitas, txWaktuAktivitas, txKerjakan;
        ImageView imgAktivitas, imgCheck;
        LinearLayout linearItemActAnak;

        public HolderData(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_item_act_anak);
            txJudulAktivitas = (TextView) itemView.findViewById(R.id.tx_judul_aktivitas_anak);
            txWaktuAktivitas = (TextView) itemView.findViewById(R.id.tx_waktu_aktivitas_anak);
            imgAktivitas = (ImageView) itemView.findViewById(R.id.img_aktivitas_anak);
            linearItemActAnak = (LinearLayout) itemView.findViewById(R.id.linear_item_act_anak);
            imgCheck = (ImageView) itemView.findViewById(R.id.img_kerjakan);
            txKerjakan = (TextView) itemView.findViewById(R.id.tx_kerjakan);
        }
    }
}
