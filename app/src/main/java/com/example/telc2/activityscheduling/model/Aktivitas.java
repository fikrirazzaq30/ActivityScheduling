package com.example.telc2.activityscheduling.model;

/**
 * Created by juvetic on 7/3/17.
 */

public class Aktivitas {

    public static final String TAG = Aktivitas.class.getSimpleName();
    public static final String TABLE = "Aktivitas";

    //Label Nama Kolom Tabel Aktivitas
    public static final String KEY_AktId = "AktId";
    public static final String KEY_AktJudul = "AktJudul";
    public static final String KEY_AktWaktu = "AktWaktu";
    public static final String KEY_Capaian = "AktCapaian";

    private String aktId;
    private String judul;
    private String waktu;
    private int capaian;

    public String getAktId() {
        return aktId;
    }

    public void setAktId(String aktId) {
        this.aktId = aktId;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public int getCapaian() {
        return capaian;
    }

    public void setCapaian(int capaian) {
        this.capaian = capaian;
    }
}
