package com.example.telc2.activityscheduling.activity.anak;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.activity.PencapaianActivity;
import com.example.telc2.activityscheduling.activity.SettingsActivity;
import com.example.telc2.activityscheduling.model.AktivitasModel;
import com.example.telc2.activityscheduling.model.response.AktivitasResponseModel;
import com.example.telc2.activityscheduling.retrofit.ApiRequest;
import com.example.telc2.activityscheduling.retrofit.RetroServer;
import com.example.telc2.activityscheduling.adapter.AktivitasAnakAdapter;
import com.example.telc2.activityscheduling.helper.RecyclerTouchListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeAnakEuyActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private List<AktivitasModel> aktivitasList;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_anak_euy);

        setTitle("");

        toolbar = (Toolbar) findViewById(R.id.tool_bar_anak);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_aktivitas_anak_euy);
        loadAktivitasAnak();


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                AktivitasModel aktivitas = aktivitasList.get(position);
                Intent intent = new Intent(HomeAnakEuyActivity.this, DetilTahapActAnakActivity.class);
                intent.putExtra("judulAktivitas", aktivitas.getAkt_judul());
                intent.putExtra("capaianAktivitas", aktivitas.getAkt_capaian());
                intent.putExtra("idAktivitas", aktivitas.getAkt_id());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);

        final SwipeRefreshLayout dorefresh = (SwipeRefreshLayout)findViewById(R.id.swp_refresh_aktivitas_home_anak);
        dorefresh.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        /*event ketika widget dijalankan*/
        dorefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override
            public void onRefresh() {
                refreshItem();
            }

            void refreshItem() {
//                populateAktivitas();
                loadAktivitasAnak();
                onItemLoad();
            }

            void onItemLoad() {
                dorefresh.setRefreshing(false);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_anak, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_pencapaian_anak) {
            Intent intent = new Intent(HomeAnakEuyActivity.this, PencapaianActivity.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.action_settings_anak) {
            Intent intent = new Intent(HomeAnakEuyActivity.this, SettingsActivity.class);
            intent.putExtra("statusOrtu", "0");
            intent.putExtra("statusAnak", "1");
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == android.R.id.home) {
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void loadAktivitasAnak() {

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mengambil data...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiRequest api = RetroServer.getClient().create(ApiRequest.class);
        Call<AktivitasResponseModel> getAktivitasAnak = api.getAktivitasAnak();
        getAktivitasAnak.enqueue(new Callback<AktivitasResponseModel>() {
            @Override
            public void onResponse(Call<AktivitasResponseModel> call, Response<AktivitasResponseModel> response) {
                progressDialog.hide();
                Log.d("Retrofit", "Response: " + response.body().getKode());
                aktivitasList = response.body().getResult();
                AktivitasAnakAdapter adapter = new AktivitasAnakAdapter(aktivitasList, getApplicationContext());
                recyclerView.setAdapter(adapter);

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<AktivitasResponseModel> call, Throwable t) {
                progressDialog.hide();
                Log.d("Retrofit", "Failure: Response Gagal");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}


