package com.example.telc2.activityscheduling.activity.ortu;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.helper.TimePickerFragment;
import com.example.telc2.activityscheduling.model.response.AktivitasResponseModel;
import com.example.telc2.activityscheduling.model.response.TahapResponseModel;
import com.example.telc2.activityscheduling.retrofit.ApiRequest;
import com.example.telc2.activityscheduling.retrofit.RetroServer;
import com.example.telc2.activityscheduling.retrofit.UploadImageResponse;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuatActAlterActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText edtJudulAktivitas, edtDeskTahap;
    private Button btnWaktu, btnSimpanAktivitas, btnTambahTahap, btnSelesai;
    private RadioGroup radioGroupNb;
    private RadioButton radioButtonNb;
    private ImageButton imbtnAktivitas, imbtnTahap;
    private TextView txTahap, txJudulAktivitasPrev, txTerapiPrev, txWaktuPrev;
    private ImageView imgAktivitasPrev;
    private LinearLayout linearTerapi;
    private RelativeLayout relativeRekam;
    private View divAktivitas1, divAktivitas2, divAktivitas3;
    private int nomortahap = 1;
    private ProgressDialog progressDialog;

    private String sId;
    private String sJudul;
    private String sWaktu;
    private String sCapaian;
    private String sTerapi;
    private String sPathGambar;
    private String sGambar;
    private File file;
    private String[] mediaColumns = { MediaStore.Video.Media._ID };
    Map<String, Integer> map = new HashMap<String, Integer>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buat_act_alter);
        setTitle("Buat Aktivitas");

        //Pair view
        edtDeskTahap = (EditText) findViewById(R.id.edt_deskr_tahap);
        edtJudulAktivitas = (EditText) findViewById(R.id.edt_judul_aktivitas);
        btnWaktu = (Button) findViewById(R.id.btn_pilih_waktu);
        btnSimpanAktivitas = (Button) findViewById(R.id.btn_simpan_aktivitas_alter);
        btnTambahTahap = (Button) findViewById(R.id.btn_simpan_tahap);
//        radioGroupNb = (RadioGroup) findViewById(R.id.radioGroupNb);
        imbtnAktivitas = (ImageButton) findViewById(R.id.imbtn_aktivitas);
        imbtnTahap = (ImageButton) findViewById(R.id.imbtn_tahap);
        txJudulAktivitasPrev = (TextView) findViewById(R.id.tx_judul_aktivitas_preview);
//        txTerapiPrev = (TextView) findViewById(R.id.tx_terapi_status_preview);
        txWaktuPrev = (TextView) findViewById(R.id.tx_waktu_aktivitas_preview);
        txTahap = (TextView) findViewById(R.id.tx_nm_tahap);
//        linearTerapi = (LinearLayout) findViewById(R.id.linearTerapi);
//        relativeRekam = (RelativeLayout) findViewById(R.id.relativeRekam);
        divAktivitas1 = findViewById(R.id.vw_divider_aktivitas1);
        divAktivitas2 = findViewById(R.id.vw_divider_aktivitas2);
        divAktivitas3 = findViewById(R.id.vw_divider_aktivitas3);
        btnSelesai = (Button) findViewById(R.id.btn_selesai_buat);

        progressDialog = new ProgressDialog(this);

        //Enable click handling
        btnSimpanAktivitas.setOnClickListener(this);
        btnTambahTahap.setOnClickListener(this);
        btnWaktu.setOnClickListener(this);
        imbtnTahap.setOnClickListener(this);
        imbtnAktivitas.setOnClickListener(this);
        btnSelesai.setOnClickListener(this);

        txTahap.setText("Tahap " + nomortahap);

        map.put("addimage", R.drawable.ic_add_camera);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public void onClick(View v) {
        if (v == findViewById(R.id.btn_pilih_waktu)) {
            showTimePickerDialog(v);
        }
        else if (v == findViewById(R.id.imbtn_aktivitas)) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, 0);
        }
        else if (v == findViewById(R.id.imbtn_tahap)) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, 0);
        }
        else if (v == findViewById(R.id.btn_simpan_tahap)) {
            if (edtDeskTahap.getText().toString().isEmpty()) {
                edtDeskTahap.setError("Tidak Boleh Kosong");
            } else {
                uploadFile();
                sGambar = "http://juvetic.telclab.com/actscdhl/uploads/" + file.getName();
                ApiRequest apiTahap = RetroServer.getClient().create(ApiRequest.class);
                Call<TahapResponseModel> insertTahap = apiTahap.insertTahap(UUID.randomUUID().toString(), Integer.toString(nomortahap), sGambar, edtDeskTahap.getText().toString(), sId);
                insertTahap.enqueue(new Callback<TahapResponseModel>() {
                    @Override
                    public void onResponse(Call<TahapResponseModel> call, Response<TahapResponseModel> response) {
                        progressDialog.dismiss();
                        Log.d("Retrofit", "response: " + response.body().toString());
                        String kode = response.body().getKode();
                        if (kode.equals("1")) {
                            nomortahap++;
                            txTahap.setText("Tahap " + nomortahap);
                            edtDeskTahap.setText("");
                            imbtnTahap.setImageResource(map.get("addimage"));

                        } else {
                            Toast.makeText(BuatActAlterActivity.this, "Data gagal disimpan", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<TahapResponseModel> call, Throwable t) {
                        progressDialog.dismiss();
                        Log.d("Retrofit", "Failure: " + "Gagal Mengirim Request" );
                    }
                });
            }
        }
         if (v == btnSimpanAktivitas) {

            if (edtJudulAktivitas.getText().toString().isEmpty()) {
                edtJudulAktivitas.setError("Tidak Boleh Kosong");
            } else {

                uploadFile();

                setTitle("Berikan Tahapan");

                imbtnTahap.setImageResource(map.get("addimage"));

                edtJudulAktivitas.setVisibility(View.GONE);
                imbtnAktivitas.setVisibility(View.GONE);
                btnWaktu.setVisibility(View.GONE);
//                linearTerapi.setVisibility(View.GONE);
                btnSimpanAktivitas.setVisibility(View.GONE);

                txJudulAktivitasPrev.setVisibility(View.VISIBLE);
//                imgAktivitasPrev.setVisibility(View.VISIBLE);
//                txTerapiPrev.setVisibility(View.VISIBLE);
                txWaktuPrev.setVisibility(View.VISIBLE);
                divAktivitas1.setVisibility(View.VISIBLE);
                divAktivitas2.setVisibility(View.VISIBLE);
                divAktivitas3.setVisibility(View.VISIBLE);

                txTahap.setVisibility(View.VISIBLE);
                imbtnTahap.setVisibility(View.VISIBLE);
                edtDeskTahap.setVisibility(View.VISIBLE);
//                relativeRekam.setVisibility(View.VISIBLE);
                btnTambahTahap.setVisibility(View.VISIBLE);
                btnSelesai.setVisibility(View.VISIBLE);

                progressDialog.setMessage("Menyimpan aktivitas...");
                progressDialog.setCancelable(false);
                progressDialog.show();

                sId = UUID.randomUUID().toString();
                sJudul = edtJudulAktivitas.getText().toString();
                sWaktu = btnWaktu.getText().toString().substring(0,5);
                sCapaian = "0";
                sTerapi = "0";

                txJudulAktivitasPrev.setText(sJudul);
                txWaktuPrev.setText("Pukul " + btnWaktu.getText().toString());

                sGambar = "http://juvetic.telclab.com/actscdhl/uploads/" + file.getName();

                ApiRequest apiAktivitas = RetroServer.getClient().create(ApiRequest.class);
                Call<AktivitasResponseModel> insertAktivitas = apiAktivitas.insertAktivitas(sId, sJudul, sGambar, sWaktu, sCapaian, sTerapi);
                insertAktivitas.enqueue(new Callback<AktivitasResponseModel>() {
                    @Override
                    public void onResponse(Call<AktivitasResponseModel> call, Response<AktivitasResponseModel> response) {
                        progressDialog.dismiss();
                        Log.d("Retrofit", "response: " + response.body().toString());
                        String kode = response.body().getKode();
                        if (kode.equals("1")) {

                        } else {
                            Toast.makeText(BuatActAlterActivity.this, "Data gagal disimpan", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<AktivitasResponseModel> call, Throwable t) {
                        progressDialog.dismiss();
                        Log.d("Retrofit", "Failure: " + "Gagal Mengirim Request" );
                    }
                });
            }
        }
        else if (v == btnSelesai) {
             finish();
         }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked
            if (requestCode == 0 && resultCode == RESULT_OK && null != data) {

                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                sPathGambar = cursor.getString(columnIndex);
                // Set the Image in ImageView for Previewing the Media
                imbtnAktivitas.setImageBitmap(BitmapFactory.decodeFile(sPathGambar));
                imbtnTahap.setImageBitmap(BitmapFactory.decodeFile(sPathGambar));
                imgAktivitasPrev.setImageBitmap(BitmapFactory.decodeFile(sPathGambar));
                cursor.close();

            } else {
//                Toast.makeText(this, "Gambar belum dipilih.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {

        }
    }

    // Uploading Image/Video
    private void uploadFile() {
        progressDialog.show();

        // Map is used to multipart the file using okhttp3.RequestBody
        file = new File(sPathGambar);

        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());

        ApiRequest getResponse = RetroServer.getClient().create(ApiRequest.class);
        Call<UploadImageResponse> call = getResponse.uploadFile(fileToUpload, filename);
        call.enqueue(new Callback<UploadImageResponse>() {
            @Override
            public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                UploadImageResponse serverResponse = response.body();
                if (serverResponse != null) {
                    if (serverResponse.getSuccess()) {

                    } else {

                    }
                } else {
                    assert serverResponse != null;
                    Log.v("Response", serverResponse.toString());
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<UploadImageResponse> call, Throwable t) {

            }
        });
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}