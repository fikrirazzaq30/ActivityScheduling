package com.example.telc2.activityscheduling.model.response;

import com.example.telc2.activityscheduling.model.TahapModel;

import java.util.List;

/**
 * Created by TELC2 on 7/9/2017.
 */

public class TahapResponseModel {

    String kode, pesan;
    List<TahapModel> result;

    public List<TahapModel> getResult() {
        return result;
    }

    public void setResult(List<TahapModel> result) {
        this.result = result;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
}