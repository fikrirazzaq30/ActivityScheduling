package com.example.telc2.activityscheduling.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.activity.anak.HomeAnakEuyActivity;
import com.example.telc2.activityscheduling.activity.ortu.HomeActivity;

public class SelesaiAktivitasActivity extends AppCompatActivity {

    private Button btnLihatPencapaian;
    private String judulAktivitas, idAktivitas, capaianAktivitas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selesai_aktivitas);

        btnLihatPencapaian = (Button) findViewById(R.id.btn_lihat_pencapaian);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null) {
            judulAktivitas = (String) b.get("judulAktivitas");
            idAktivitas = (String) b.get("idAktivitas");
            capaianAktivitas = (String) b.get("capAktivitas");
        }

        setTitle("Aktivitas " + judulAktivitas);

        btnLihatPencapaian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelesaiAktivitasActivity.this, DetilPencapaianActivity.class);
                intent.putExtra("judulAktivitas", judulAktivitas);
                intent.putExtra("idAktivitas", idAktivitas);
                intent.putExtra("capAktivitas", capaianAktivitas);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}